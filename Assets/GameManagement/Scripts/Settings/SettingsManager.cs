﻿using UnityEngine;

namespace Wild.Freelance.GameManagement.Settings
{
    public class SettingsManager
    {
        private static readonly string _soundKey = "GameManagement.Settings.SoundVolume";

        public SettingsManager()
        {
            SoundVolume = PlayerPrefs.GetFloat(_soundKey, 1f);
        }

        private float _soundVolume;
        public float SoundVolume
        {
            get { return _soundVolume; }
            set
            {
                _soundVolume = value;
                PlayerPrefs.SetFloat(_soundKey, _soundVolume);
                AudioListener.volume = _soundVolume;
            }
        }
    }
}
