﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Wild.Freelance.GameManagement.Levels
{
    public class LevelManager
    {
        public LevelManager(string pathLevelsData)
        {
            LevelsData data = Resources.Load<LevelsData>(pathLevelsData);
            MainMenuId = data.MainMenuId;

            _levels.Add(MainMenuId);
            _levels.AddRange(data.LevelIds);
        }
        public readonly int MainMenuId;

        private readonly List<int> _levels = new List<int>();

        public int CurrentLevelId => SceneManager.GetActiveScene().buildIndex;
        public int CurrentSceneId => SceneManager.GetActiveScene().buildIndex;
        public int? GetLevelId(int id) => _levels.Contains(id) ? (int?)id : null;
        public int GetSceneBuildIndex(int levelId) => SceneManager.GetSceneByBuildIndex(levelId).buildIndex;
        public Scene GetScene(int index) => SceneManager.GetSceneByBuildIndex(index);
        public int? GetNextLevelId()
        {
            int nextLevelId = CurrentLevelId + 1;
            return _levels.Count > nextLevelId ? (int?)nextLevelId : null;
        }

        public void LoadMainMenu() => LoadScene(MainMenuId);
        public void LoadLevel(int id) => LoadScene(id);
        public void LoadScene(int id)
        {
            SceneManager.LoadScene(id);
            OnSceneReload?.Invoke();
        }
        public void LoadNextLevel() => LoadLevel(GetNextLevelId().Value);

        public void LoadMainMenuAsync() => LoadSceneAsync(MainMenuId);
        public void LoadLevelAsync(int levelId) => LoadSceneAsync(levelId);
        public void LoadSceneAsync(int id)
        {
            OnSceneLoadBegin?.Invoke();
            SceneManager.LoadSceneAsync(id);
            SceneManager.sceneLoaded += CallOnSceneLoadEnd;
        }
        /// <summary>
        /// Вызывается перед загрузкой сцены
        /// </summary>
        public event Action OnSceneLoadBegin;
        /// <summary>
        /// Вызывается после загрузки сцены
        /// </summary>
        public event Action OnSceneLoadEnd;

        private void CallOnSceneLoadEnd(Scene scene, LoadSceneMode loadSceneMode)
        {
            OnSceneLoadEnd?.Invoke();
            SceneManager.sceneLoaded -= CallOnSceneLoadEnd;

            OnSceneReload?.Invoke();
        }

        /// <summary>
        /// Вызывать для перезагрузки сцены
        /// </summary>
        public void ReloadScene() => LoadSceneAsync(CurrentLevelId);
        public void ReloadLevel() => ReloadScene();

        public event Action OnSceneReload;
    }
}
