﻿using UnityEngine;
using Wild.Freelance.GameManagement.InVectorManagement;
using Wild.Freelance.GameManagement.Savers;

namespace Wild.Freelance.GameManagement.Levels
{
    [RequireComponent(typeof(BoxCollider))]
    public class LevelChanger : MonoBehaviour
    {
        [SerializeField]
        private string _playerTag = "Player";

        private void Start()
        {
            GetComponent<BoxCollider>().isTrigger = true;
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            if (!otherCollider.CompareTag(_playerTag))
                return;

            if (!GameManager.LevelManager.GetNextLevelId().HasValue)
                return;

            var saveItem = Saver.AddSave(WildInVectorManager.GetInventoryItems(), GameManager.LevelManager.GetNextLevelId().Value);
            WildInVectorManager.RemoveAllGameObjects();

            GameManager.LevelManager.LoadLevel(saveItem.LevelId);
            WildInVectorManager.LastInventoryItems = saveItem.Items;
        }
    }
}
