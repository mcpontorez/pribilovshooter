﻿using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Wild.Freelance.GameManagement.Levels
{
    [CreateAssetMenu(fileName = "LevelsData", menuName = "GameManagement/LevelManagement/LevelsData")]
    public class LevelsData : ScriptableObject
    {
        [Header("Сюда перетаскивать сцены")]
#if UNITY_EDITOR
        [SerializeField]
        private SceneAsset _mainMenu;
#endif
        [HideInInspector]
        [SerializeField]
        private int _mainMenuId;
        public int MainMenuId { get { return _mainMenuId; } }
#if UNITY_EDITOR
        [SerializeField]
        private List<SceneAsset> _levelsForEditor = new List<SceneAsset>();
#endif
        [Header("НЕ ТРОГАТЬ")]
        [SerializeField]
        private List<int> _levelsIds = new List<int>();
        public List<int> LevelIds => _levelsIds;

#if UNITY_EDITOR
        private void OnValidate()
        {
            AddScenesToBuild();
        }

        [ContextMenu("Setup")]
        private bool AddScenesToBuild()
        {
            List<EditorBuildSettingsScene> buildScenes = new List<EditorBuildSettingsScene>();

            if (!_mainMenu)
            {
                Debug.LogWarning("Добавьте сцену Main Menu!");
                return false;
            }
            buildScenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(_mainMenu), true));
            _mainMenuId = 0;

            _levelsIds.Clear();

            foreach (var levelForEditor in _levelsForEditor)
            {
                if (!levelForEditor)
                    continue;

                int i = buildScenes.FindIndex(x => x.path == AssetDatabase.GetAssetPath(levelForEditor));
                if (i < 0)
                {
                    buildScenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(levelForEditor), true));
                    i = buildScenes.Count - 1;
                }
                else if (i == 0)
                {
                    Debug.LogException(new System.Exception("Игровой уровень " + levelForEditor.name + " в расположении " + AssetDatabase.GetAssetPath(levelForEditor) + " не должен являться Main Menu!"));
                    return false;
                }
                else
                    Debug.LogWarning("Уровень " + AssetDatabase.GetAssetPath(levelForEditor) + " добавлен больше одного раза!");

                _levelsIds.Add(i);
            }

            EditorBuildSettings.scenes = buildScenes.ToArray();

            return true;
        }
#endif
    }
}
