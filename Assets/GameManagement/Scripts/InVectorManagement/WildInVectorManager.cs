﻿using Invector;
using Invector.ItemManager;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Wild.Freelance.GameManagement.InVectorManagement
{
    /// <summary>
    /// костыли для костылей InVector
    /// </summary>
    public static class WildInVectorManager
    {
        private static Systems.IGameLogicUpdateSystem GameLogicUpdateSystem => GameManager.GameLogicUpdateSystem;

        public static List<ItemReference> LastInventoryItems { get; set; } = new List<ItemReference>();

        public static void RemoveAllGameObjects(bool isRemovePlayerBody = true)
        {
            Object.FindObjectsOfType<vMonoBehaviour>().ToList().ForEach(v => Object.Destroy(v.gameObject));
            Object.Destroy(Object.FindObjectOfType<vGameController>().gameObject);
            Object.Destroy(InventoryInstance.gameObject);
            if(isRemovePlayerBody)
                GameObject.FindGameObjectsWithTag("Player").ToList().ForEach(v => Object.Destroy(v.gameObject));
        }

        public static vInventory InventoryInstance { get; set; }
        public static void SetInventoryEnable(bool isActive)
        {
            InventoryInstance.gameObject.SetActive(isActive);
        }
        public static bool GetInventoryIsOpen()
            => InventoryInstance.isOpen;

        public static List<ItemReference> GetInventoryItems()
        {
            vItemManager itemManager = Object.FindObjectOfType<vItemManager>();

            return itemManager.items.Select(item => new ItemReference(item.id)
            {
                amount = item.amount,
                attributes = item.attributes,
                autoEquip = item.isInEquipArea,
                //indexArea = item.EquipID,
            }).ToList();
        }
        public static void AddInventoryItems(List<ItemReference> items, vItemManager itemManager = null)
        {
            if (items == null || items.Count == 0)
                return;

            if(!itemManager)
                itemManager = Object.FindObjectOfType<vItemManager>();

            items.ForEach(item => itemManager.AddItem(item, true));

            LastInventoryItems = items;
        }
        public static void AddInventoryItemsLazy(List<ItemReference> items)
        {
            GameLogicUpdateSystem.StartCoroutine(AddInventoryItemsLazyRaw(items));
        }
        public static IEnumerator AddInventoryItemsLazyRaw(List<ItemReference> items)
        {
            vItemManager itemManager = null;
            while (!itemManager)
            {
                yield return new WaitForEndOfFrame();
                itemManager = Object.FindObjectOfType<vItemManager>();               
            }
            AddInventoryItems(items, itemManager);
        }

        public static void AddLastInventoryItemsLazy()
        {
            AddInventoryItemsLazy(LastInventoryItems);
        }

        public static void ReloadGameLazy()
        {
            GameLogicUpdateSystem.StartCoroutine(ReloadGameLazyRaw());
        }
        public static IEnumerator ReloadGameLazyRaw()
        {
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForEndOfFrame();
            }
            RemoveAllGameObjects(false);
            GameManager.LevelManager.ReloadLevel();
        }
    }
}
