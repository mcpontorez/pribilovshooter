﻿using Invector;
using Invector.ItemManager;
using System.Collections;
using UnityEngine;

namespace Wild.Freelance.GameManagement.InVectorManagement
{
    public class WildInVectorManagerMB : MonoBehaviour
    {
        private IEnumerator Start()
        {
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForEndOfFrame();
            }
            WildInVectorManager.InventoryInstance = FindObjectOfType<vInventory>();
            WildInVectorManager.AddLastInventoryItemsLazy();

            vGameController.instance.OnReloadGame.AddListener(WildInVectorManager.ReloadGameLazy);
        }
    }
}
