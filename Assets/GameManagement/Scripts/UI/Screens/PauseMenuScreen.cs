﻿using UnityEngine;
using System.Linq;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public sealed class PauseMenuScreen : ScreenBase<PauseMenuScreenBehaviour>
    {
        protected override string DataPath => "GameManagement/UI/Screens/PauseMenuScreen";

        protected override void OnInit()
        {
            base.OnInit();
            ScreenBehaviour.ResumeGameButton.onClick.AddListener(ResumeGame);
            ScreenBehaviour.BackToMainMenuButton.onClick.AddListener(BackToMainMenuButton);
        }

        protected override void OnShow()
        {
            base.OnShow();
            GameManager.Status = GameStatuses.Pause;

            InVectorManagement.WildInVectorManager.SetInventoryEnable(false);
        }

        private void ResumeGame()
        {
            HideShow<GameplayScreen>();
            InVectorManagement.WildInVectorManager.SetInventoryEnable(true);
        }

        private void BackToMainMenuButton()
        {
            InVectorManagement.WildInVectorManager.RemoveAllGameObjects();

            GameManager.LevelManager.LoadMainMenu();
            HideShow<MainMenuScreen>();
        }
    }
}
