﻿using UnityEngine;
using UnityEngine.UI;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public class PauseMenuScreenBehaviour : ScreenBehaviourBase
    {
        [SerializeField]
        private Button _resumeGameButton;
        public Button ResumeGameButton => _resumeGameButton;

        [SerializeField]
        private Button _backToMainMenuButton;
        public Button BackToMainMenuButton => _backToMainMenuButton;
    }
}
