﻿using Wild.Systems;
using Wild.UI.ScreenManagement;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public abstract class ScreenBase<TScreenBehaviour> : ScreenBase where TScreenBehaviour : ScreenBehaviourBase
    {
        protected IGameLogicUpdateSystem GameLogicUpdateSystem { get; private set; }

        protected TScreenBehaviour ScreenBehaviour { get; private set; }
        protected override void OnInit()
        {
            base.OnInit();
            GameLogicUpdateSystem = ScreenManager.SystemManager.GetSystem<GameLogicUpdateSystem>();
            ScreenBehaviour = Data.GetComponent<TScreenBehaviour>();
        }
    }
}
