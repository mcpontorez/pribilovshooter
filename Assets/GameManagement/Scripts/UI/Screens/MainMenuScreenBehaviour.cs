﻿using UnityEngine;
using UnityEngine.UI;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public class MainMenuScreenBehaviour : ScreenBehaviourBase
    {
        [SerializeField]
        private Button _newGameButton;
        public Button NewGameButton => _newGameButton;

        [SerializeField]
        private Button _savesButton;
        public Button SavesButton => _savesButton;

        [SerializeField]
        private Button _settingsButton;
        public Button SettingsButton => _settingsButton;

        [SerializeField]
        private Button _exitButton;
        public Button ExitButton => _exitButton;
    }
}
