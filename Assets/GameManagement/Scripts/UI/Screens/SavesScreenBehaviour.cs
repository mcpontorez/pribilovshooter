﻿using UnityEngine;
using UnityEngine.UI;
using Wild.Freelance.GameManagement.UI.Components;
using Wild.UI.Components;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public class SavesScreenBehaviour : ScreenBehaviourBase
    {
        [SerializeField]
        private SavedGameInfoItem _savedGameInfoItemTemplate;
        public SavedGameInfoItem SavedGameInfoItemTemplate => _savedGameInfoItemTemplate;

        [SerializeField]
        private CollectionViewController _savedGamesList;
        public ICollectionView SavedGamesList => _savedGamesList;

        [SerializeField]
        private Button _deleteAllSavedGamesButton;
        public Button DeleteAllSavedGamesButton => _deleteAllSavedGamesButton;

        [SerializeField]
        private Button _backToMainMenuButton;
        public Button BackToMainMenuButton => _backToMainMenuButton;
    }
}
