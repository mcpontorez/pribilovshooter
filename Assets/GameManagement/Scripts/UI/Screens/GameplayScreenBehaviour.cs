﻿using UnityEngine;
using UnityEngine.UI;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public class GameplayScreenBehaviour : ScreenBehaviourBase
    {
        [SerializeField]
        private Text _infoText;
        public Text InfoText => _infoText;
    }
}
