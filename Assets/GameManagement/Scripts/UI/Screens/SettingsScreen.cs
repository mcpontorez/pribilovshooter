﻿
namespace Wild.Freelance.GameManagement.UI.Screens
{
    public sealed class SettingsScreen : ScreenBase<SettingsScreenBehaviour>
    {
        protected override string DataPath => "GameManagement/UI/Screens/SettingsScreen";

        protected override void OnInit()
        {
            base.OnInit();

            ScreenBehaviour.BackToMainMenuButton.onClick.AddListener(BackToMainMenu);
            ScreenBehaviour.SoundSlider.OnValueChanged += (volume) => GameManager.SettingsManager.SoundVolume = volume;
        }

        protected override void OnShow()
        {
            base.OnShow();
            ScreenBehaviour.SoundSlider.Value = GameManager.SettingsManager.SoundVolume;
        }

        private void BackToMainMenu() => HideShow<MainMenuScreen>();
    }
}
