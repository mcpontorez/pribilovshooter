﻿using UnityEngine;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public sealed class MainMenuScreen : ScreenBase<MainMenuScreenBehaviour>
    {
        protected override string DataPath => "GameManagement/UI/Screens/MainMenuScreen";

        protected override void OnInit()
        {
            base.OnInit();
            ScreenBehaviour.NewGameButton.onClick.AddListener(NewGame);
            ScreenBehaviour.SavesButton.onClick.AddListener(ShowSavesScreen);
            ScreenBehaviour.SettingsButton.onClick.AddListener(ShowSettingsScreen);
            ScreenBehaviour.ExitButton.onClick.AddListener(ExitFromGame);
        }

        protected override void OnShow()
        {
            base.OnShow();
            InVectorManagement.WildInVectorManager.LastInventoryItems.Clear();
            GameManager.Status = GameStatuses.Menu;
        }

        private void NewGame()
        {
            GameManager.LevelManager.LoadLevel(1);
            HideShow<GameplayScreen>();
        }

        private void ShowSavesScreen() => HideShow<SavesScreen>();

        private void ShowSettingsScreen() => HideShow<SettingsScreen>();

        private void ExitFromGame()
        {
            Hide();
            Application.Quit();
        }
    }
}
