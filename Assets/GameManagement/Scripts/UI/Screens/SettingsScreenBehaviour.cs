﻿using UnityEngine;
using UnityEngine.UI;
using Wild.UI.Components;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public class SettingsScreenBehaviour : ScreenBehaviourBase
    {
        [SerializeField]
        private SliderController _soundSlider;
        public ISlider SoundSlider => _soundSlider;

        [SerializeField]
        private Button _backToMainMenuButton;
        public Button BackToMainMenuButton => _backToMainMenuButton;
    }
}
