﻿using System.Collections;
using UnityEngine;
using Wild.Freelance.GameManagement.Savers;
using Wild.Systems;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public sealed class GameplayScreen : ScreenBase<GameplayScreenBehaviour>
    {
        protected override string DataPath => "GameManagement/UI/Screens/GameplayScreen";

        public string InfoText { get { return ScreenBehaviour.InfoText.text; } set { ScreenBehaviour.InfoText.text = value; } }

        protected override void OnInit()
        {
            base.OnInit();
            InfoText = string.Empty;
        }

        protected override void OnShow()
        {
            base.OnShow();
            GameManager.Status = GameStatuses.Play;
            GameLogicUpdateSystem.OnUpdated += GameLogicUpdateSystem_OnUpdated;

            Saver.OnSaved += Saver_OnSaved;
        }

        private void Saver_OnSaved()
        {
            GameLogicUpdateSystem.StartCoroutine(ShowSavedText());
        }

        private IEnumerator ShowSavedText()
        {
            InfoText = "Игра сохранена";
            yield return new WaitForSeconds(3f);
            InfoText = string.Empty;
        }

        private void GameLogicUpdateSystem_OnUpdated()
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !InVectorManagement.WildInVectorManager.GetInventoryIsOpen())
                HideShow<PauseMenuScreen>();
        }

        protected override void OnHide()
        {
            base.OnHide();
            GameLogicUpdateSystem.OnUpdated -= GameLogicUpdateSystem_OnUpdated;
            Saver.OnSaved -= Saver_OnSaved;
        }
    }
}
