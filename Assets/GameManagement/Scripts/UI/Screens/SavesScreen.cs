﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using Wild.Freelance.GameManagement.Savers;

namespace Wild.Freelance.GameManagement.UI.Screens
{
    public sealed class SavesScreen : ScreenBase<SavesScreenBehaviour>
    {
        protected override string DataPath => "GameManagement/UI/Screens/SavesScreen";

        protected override void OnInit()
        {
            base.OnInit();

            ScreenBehaviour.DeleteAllSavedGamesButton.onClick.AddListener(DeleteAll);
            ScreenBehaviour.BackToMainMenuButton.onClick.AddListener(BackToMainMenu);
        }

        protected override void OnShow()
        {
            base.OnShow();
            UpdateSavedGamesList();
        }

        private void UpdateSavedGamesList()
        {
            List<SavesItem> savesItems = Saver.LoadSaves();
            savesItems = savesItems.OrderByDescending(s => s.DateTimeText).ToList();

            ScreenBehaviour.SavedGamesList.SetItems(ScreenBehaviour.SavedGameInfoItemTemplate, savesItems.Count, (infoItem, index) => {
                SavesItem saveItem = savesItems[index];                   
                infoItem.DateInfo = saveItem.DateTimeText;
                infoItem.LevelInfo = "Level " +  saveItem.LevelId;
                infoItem.OnClick += () =>
                {
                    GameManager.LevelManager.LoadLevel(saveItem.LevelId);
                    HideShow<GameplayScreen>();
                    InVectorManagement.WildInVectorManager.LastInventoryItems = saveItem.Items;
                };
            });
        }

        private void DeleteAll()
        {
            Saver.DeleteAllFromStorage();
            UpdateSavedGamesList();
        }

        private void BackToMainMenu() => HideShow<MainMenuScreen>();
    }
}
