﻿using Wild.UI.Components;

namespace Wild.Freelance.GameManagement.UI.Components
{
    public interface ISavedGameInfoItem : IClickable
    {
        string LevelInfo { get; set; }

        string DateInfo { get; set; }
    }
}
