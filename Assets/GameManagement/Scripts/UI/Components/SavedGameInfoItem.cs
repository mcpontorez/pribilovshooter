﻿using UnityEngine;
using UnityEngine.UI;
using Wild.UI.Components;

namespace Wild.Freelance.GameManagement.UI.Components
{
    public class SavedGameInfoItem : Clickable, ISavedGameInfoItem
    {
        [SerializeField]
        private Text LevelInfoTextComponent;
        public string LevelInfo { get { return LevelInfoTextComponent.text; } set { LevelInfoTextComponent.text = value; } }

        [SerializeField]
        private Text DateInfoTextComponent;
        public string DateInfo { get { return DateInfoTextComponent.text; } set { DateInfoTextComponent.text = value; } }
    }
}
