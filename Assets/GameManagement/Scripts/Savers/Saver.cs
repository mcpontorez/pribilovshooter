﻿using Invector.ItemManager;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Wild.Freelance.GameManagement.Savers
{
    public static class Saver
    {
        public static string FolderPath => Application.persistentDataPath + "/WildSaves";
        private static string FileExist => ".wildsave";

        public static SavesItem AddSave(List<ItemReference> items, int levelId)
        {
            SavesItem savesItem = new SavesItem() {
                Items = items, LevelId = levelId, DateTime = DateTime.Now,
            };
            
            string json = JsonUtility.ToJson(savesItem);
            SaveOnStorage(savesItem.ToString(), json);

            OnSaved?.Invoke();

            return savesItem;
        }
        private static void SaveOnStorage(string fileName, string json)
        {
            if (!Directory.Exists(FolderPath))
                Directory.CreateDirectory(FolderPath);
            string filePath = FolderPath + "/" + fileName + FileExist;

            File.WriteAllText(filePath, json);
        }

        public static List<SavesItem> LoadSaves()
        {
            List<SavesItem> saves = new List<SavesItem>();

            if (!Directory.Exists(FolderPath))
                return saves;

            foreach (string item in Directory.GetFiles(FolderPath))
            {
                try
                {
                    string json = File.ReadAllText(item);
                    if (string.IsNullOrWhiteSpace(json))
                        throw new FileLoadException("save file is empty");

                    SavesItem savesItem = JsonUtility.FromJson<SavesItem>(json);
                    saves.Add(savesItem);
                }
                catch
                {
                    File.Delete(item);
                }
            }
            return saves;
        }

        public static void DeleteAllFromStorage()
        {
            if (!Directory.Exists(FolderPath))
                return;

            foreach (string item in Directory.GetFiles(FolderPath))
            {
                File.Delete(item);
            }
        }

        public static event Action OnSaved;
    }
}
