﻿using Invector.ItemManager;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Wild.Freelance.GameManagement.Savers
{
    [Serializable]
    public class SavesItem
    {
        private static readonly string _timeFormat = "yyyy-dd-MMTHH-mm-ss";

        public int LevelId = 2;
        public List<ItemReference> Items = new List<ItemReference>();
        [SerializeField]
        private string _dateTimeText;
        public string DateTimeText => _dateTimeText;
        public DateTime DateTime { set { _dateTimeText = value.ToString(_timeFormat); } }

        public override string ToString() => $"{DateTimeText}_{nameof(LevelId)}_{LevelId}";
    }
}
