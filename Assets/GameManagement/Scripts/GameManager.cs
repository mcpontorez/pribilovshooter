﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using Wild.Freelance.GameManagement.Settings;
using Wild.Freelance.GameManagement.UI.Screens;
using Wild.Systems;
using Wild.Systems.Management;
using Wild.UI.ScreenManagement;

namespace Wild.Freelance.GameManagement
{
    public static class GameManager
    {
        [RuntimeInitializeOnLoadMethod]
        public static void Initialize()
        {
            ScreenManager.EventSystem = EventSystem;

            if (LevelManager.CurrentSceneId == LevelManager.MainMenuId)
                ScreenManager.ShowScreen<MainMenuScreen>();
            else
                ScreenManager.ShowScreen<GameplayScreen>();
        }

        public static SettingsManager SettingsManager { get; private set; } = new SettingsManager();

        public static IScreenManager ScreenManager { get; private set; } = new ScreenManager();

        public static SystemManager SystemManager { get; private set; } = new SystemManager();
        public static EventSystem EventSystem { get; private set; } = SystemManager.LoadAndAddSystem<EventSystem>("GameManagement/EventSystem");
        public static IGameLogicUpdateSystem GameLogicUpdateSystem { get; private set; } = SystemManager.GetSystem<GameLogicUpdateSystem>();

        public static Levels.LevelManager LevelManager { get; private set; } = new Levels.LevelManager("GameManagement/Levels/LevelsData");

        private static GameStatuses _status = GameStatuses.Menu;
        public static GameStatuses Status
        {
            get { return _status; }
            set
            {
                if (value == _status)
                    return;
                _status = value;
                switch (_status)
                {
                    case GameStatuses.Menu:
                        Time.timeScale = 0.0005f;
                        break;
                    case GameStatuses.Pause:
                        Time.timeScale = 0.0005f;
                        break;
                    case GameStatuses.Play:
                        Time.timeScale = 1f;
                        break;
                }

                OnGameStatusChanget?.Invoke(_status);
            }
        }

        public static event Action<GameStatuses> OnGameStatusChanget;
    }
}