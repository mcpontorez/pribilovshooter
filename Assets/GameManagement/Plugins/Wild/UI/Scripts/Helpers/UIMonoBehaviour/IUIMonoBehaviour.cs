﻿using UnityEngine;namespace Wild.UI.Helpers
{
    public interface IUIMonoBehaviour
    {
        RectTransform rectTransform { get; }
    }
}